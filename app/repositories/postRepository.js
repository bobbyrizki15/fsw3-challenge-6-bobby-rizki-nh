const { Users, Cars } = require("../models");

module.exports = {
  register(data) {
    return Users.create(data);
  },

  findByEmail(email) {
    return Users.findOne({
      where: { email }
    });
  },

  create(data) {
    return Cars.create(data);
  }
};
